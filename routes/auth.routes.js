const express = require('express');
const controller = require('../controller/auth.controller');

const router = express.Router();

router.get("/register", controler.registerGet);
router.post("/register", controller.registerPost);
