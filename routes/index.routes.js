const express = require('express');
const User = require('../models/User.model')

const router = express.Router();

router.get('/',(req, res, next) => {
    console.log('On index route');
    return res.status(200).json('index route');
});

router.get('/save-user', async (req, res, next) => {

    try {
        const newUser = new User({
            name:"Javier Galán",
            password:"tuvieja",
            email:"tuvieja@tuvieja.com",
            isAdmin: true,
        });
    
        const user = await newUser.save();

        console.log(user)
        return res.json(user);
    } catch (error) {
        console.log('error: ', error);     
    }
});

module.exports = router;