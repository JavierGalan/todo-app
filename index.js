const express = require('express');
const dotenv = require('dotenv');
dotenv.config();
const path = require('path'); 
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');
// autentificacion
const auth = require('./auth');
auth.setStrategies();
// Mehtod Override
const methodOverride = require('method-override');
const PORT = process.env.PORT || 3000;
const db = require('./config/db.congfig');
db.connect();

const indexRoutes = require('./routes/index.routes');

const app = express();

// configuracion de la cookie de sesion
// app.use(
//   session({
//     key: 'no se que se pone aquí',
//     secret: process.env["SESSION_SECRET"],
//     resave: false,
//     saveUninitialized: false,
//     cookie: {
//       maxAge: 24 * 60 * 60 * 1000,
//     },
//     store: MongoStore.create({ mongoUrl: db.DB_URL }),
//   })
// );

app.use(passport.initialize());

app.use(express.static(path.join(__dirname, 'public')));

// rutas
app.use('/', indexRoutes);
app.use('*', (req, res, next) =>{
    const error = new Error('Route not found');
    error.status = 404;
    return res.json(error.message);
});

app.use((error, req, res, next) =>{
    return res.json({message: error.message || 'Unexpected Error',
     status: error.status || 500
    });
});

app.listen(PORT,()=>{
    console.log(`Servidor funcionando en http://localhost:${PORT}`);
});