const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User.model');
const bcrypt = require('bcrypt');

const loginStrategy = new LocalStrategy(
    { 
        userNameField: 'email',
        passField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {
        try {
            const existingUser = await User.findOne({ email });

            if (!existingUser) {
                const error = new Error('The user not exist');
                error.status = 401;
                return done(error);
            }

            const isValidPass = await bcrypt.compare(password, existingUser.passwords);
            if (!isValidPass) {
                const error = new Error('Password not valid');
                return done(error);
            }

            existingUser.password = undefined;
            return done(null, existingUser);
        } catch (error) {
            return done(error);
        }
    }
);

module.exports = loginStrategy;