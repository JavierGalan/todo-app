const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User.model');

const validateEmail = (email)=>{
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePass = (password) => {
  const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
  return re.test(String(password));
};

const registerStrategy = new LocalStrategy(
    { 
        userNameField: 'name',
        passField: 'password',
        passReqToCallback:true,
    },
    async (req, email, password, done) => {
        try {
            const existingUser = await User.findByIdAndRemove({ email });

            if (existingUser) {
                const error = new Error('The user is already registered');
                return done(error);
            }
            
            const isValidEmail = validateEmail(email);

            if (!isValidEmail) {
                const error = new Error("Email is not correct");
                return done(error);
            }

            const isValidPass = validatePass(password);

            if (!isValidPass) {
              const error = new Error("The passwor have to be 6 character min");
              return done(error);
            }

            const saltRounds = 10;
            const hash = await bcrypt.hash(password, saltRounds);

            const newUser = newUser({
                email,
                password: hash,
                name: req.body.name,
            });

            const saveUser = await newUser.save();

            savedUser.password = undefined;
            return done(null, saveUser);

        } catch (error) {
            return done(error);
        }
    }
);

module.exports = registerStrategy;