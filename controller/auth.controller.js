const passport = require('passport');

const registerGet = (req, res) => {
    return res.render("./auth/register")
};

const registerPost = (req, res, next) => {
    const done = (error, user)=>{
        if (error) {
            return next(error);
        }

        req.logIn(user,(error)=>{
            if (error) {
                return next(error);
            }

            return res.redirect("/")
        });
    }
};