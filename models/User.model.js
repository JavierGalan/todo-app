const mongoose = require('mongoose');

const {Schema} = mongoose;

const userSchema = new Schema(
  {
    email: { type: String, required: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    avatar: {
      type: String,
      default:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png",
    },
    isAdmin: { type: Boolean, default: false, required: true },
    todos: { type: String },
  },
  { timestamp: true }
);

const User = mongoose.model('User', userSchema);

module.exports = User;